## Run commands in this order: ##

- pre-install.sh
- run.sh

## The ansible script will install the following: ##

### Packages: ###
- alsa-utils
- apt-transport-https
- build-essential
- ca-certificates
- curl
- dnsutils
- git
- htop
- lynx
- nano
- net-tools
- bind9utils
- python3-pip
- ssh
- wget
- ansible
- docker-ce
- spotify-client
- code-insiders
- pulseeffects

### Repo keys: ###
- https://packages.microsoft.com/keys/microsoft.asc
- http://nginx.org/keys/nginx_signing.key
- https://download.docker.com/linux/ubuntu/gpg
- https://deb.nodesource.com/gpgkey/nodesource.gpg.key
- http://repo.dumalogiya.ru/keys/mikhailnov_pub.gpg #PulseEffects
- spotify
- mariadb
- hhvm
- updated graphics-drvers

### Repos: ###
- deb [arch=amd64] http://packages.microsoft.com/repos/vscode stable main
- deb http://ppa.launchpad.net/graphics-drivers/ppa/ubuntu {{ ansible_distribution_release }} main
- "ppa:graphics-drivers/ppa"
- deb http://repository.spotify.com stable non-free
- deb [arch=amd64] https://download.docker.com/linux/ubuntu {{ ansible_distribution_release }} edge
- deb https://deb.nodesource.com/node_8.x {{ ansible_distribution_release }} main
- deb http://repo.dumalogiya.ru/aptly/public artful main #PulseEffects

### Visual Studios Code Extensions: ###
- HookyQR.beautify
- Tyriar.sort-lines
- PeterJausovec.vscode-docker
- vscoss.vscode-ansible
- ms-kubernetes-tools.vscode-kubernetes-tools